FROM pierrezemb/gostatic
 
# Bundle app source
COPY ./dist/build /srv/http
WORKDIR /
EXPOSE  8043
ENTRYPOINT ["/goStatic"]