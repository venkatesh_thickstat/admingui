'use strict';
define(['angular', 'controllers', 'services'], function(angular) {

	var app = angular.module("tsApp", ['ui.router', 'ui.bootstrap', 'ui.grid', 'ui.grid.resizeColumns', 'ui.grid.edit', 'blockUI', 'ngAnimate', 'controllerModules', 'serviceModules', 'restangular', 'angularValidator', 'angularFileUpload', 'ngTagsInput', 'ngDomToPdf']);

	angular.element.notifyDefaults({
		newest_on_top: true,
		allow_dismiss: true,
		delay: 5000,
		placement: {
			align: "center"
		},
		animate: {
			enter: 'animated rollIn',
			exit: 'animated rollOut'
		},
		template: '<div data-notify="container" class="col-xs-4 col-sm-3 notification-{0}">' +
			'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
			'<span data-notify="title">{1}</span>' +
			'<span data-notify="message">{2}</span>' +
			'</div>'
	});

	//This is to verify the authenciation status before changing the application state
	var verifyAuth = ['$q', '$log', 'authService', function($q, $log, authService) {
		var authenticated = authService.isAuthenticated();
		if (authenticated) {
			return $q.when(authenticated);
		} else {
			$log.error("Routing is not allowed due to authentication failure");
			return $q.reject({
				authstatus: false
			});
		}
	}];

	app.controller("indexController", ['$scope', function($scope) {
		$scope.isAuthenticated = true;
	}]);

	app.filter('join', function() {
		return function join(array, separator, prop) {
			if (!Array.isArray(array)) {
				return array; // if not array return original - can also throw error
			}

			return (!!prop ? array.map(function(item) {
				return item[prop];
			}) : array).join(separator);
		};
	});

	//Directives
	app.directive('schrollBottom', function() {
		return {
			scope: {
				schrollBottom: "="
			},
			link: function(scope, element) {
				scope.$watchCollection('schrollBottom', function(newValue) {
					if (newValue) {
						angular.element(element).scrollTop(angular.element(element)[0].scrollHeight);
					}
				});
			}
		};
	});

	app.directive('onEnterKeyPress', function() {
		return function(scope, element, attrs) {
			element.bind("keydown keypress", function(event) {
				if (event.which === 13) {
					scope.$apply(function() {
						scope.$eval(attrs.onEnterKeyPress);
					});

					event.preventDefault();
				}
			});
		};
	});

	app.directive('execOnScrollToTop', function() {

		return {

			restrict: 'A',
			link: function(scope, element, attrs) {
				var fn = scope.$eval(attrs.execOnScrollToTop);
				element.on('scroll', function(e) {
					if (!e.target.scrollTop) {
						scope.$apply(fn);
					}
				});
			}
		};

	});

	app.run(['configService', '$log', 'Restangular', '$rootScope', '$state', '$window',
		function(configService, $log, Restangular, $rootScope, $state, $window) {
			//Initialize config service
			configService.initialize();
			//Initializing Restangular with base URL
			Restangular.setBaseUrl(configService.getApiURL());
			$log.info("Base URL has been set as :" + configService.getApiURL());

			$rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
				return $state.go("login");
			});

			if ($window.sessionStorage.auth) {
				var auth = JSON.parse(($window.sessionStorage.auth));

				$rootScope.userRole = auth.data.role;
				$rootScope.orgid = auth.data.orgid;
				var headers = {
					"Authorization": auth.data.token
				};
				Restangular.setDefaultHeaders(headers);
			}

			//Configure restangular to get the original response
			//This is done to avoid restangular intelligence scattered across the application
			//The service layer should take care of returning the original element always
			Restangular.setResponseExtractor(function(response) {
				var newResponse = response;
				if (newResponse) {
					if (angular.isArray(response)) {
						newResponse.originalElement = [];
						angular.forEach(newResponse, function(value, key) {
							newResponse.originalElement[key] = angular.copy(value);
						});
					} else {
						newResponse.originalElement = angular.copy(response);
					}
				}
				return newResponse;
			});
		}
	]);

	//Setting the route configuration based on the state, using the ui-router
	app.config(["$stateProvider", "$urlRouterProvider", "blockUIConfig",
		function($stateProvider, $urlRouterProvider, blockUIConfig) {

			blockUIConfig.delay = 0;
			blockUIConfig.autoBlock = false;
			blockUIConfig.template = '<div class="block-ui-overlay"></div><div class="block-ui-message-container"><div class="" ng-class="$_blockUiMessageClass"><div><i class="loading"></i></div><strong>Please wait...</strong></div></div>';

			//setting the router configuration
			$urlRouterProvider.otherwise("/login");

			$stateProvider.state("login", {
				url: "/login",
				views: {
					"login": {
						templateUrl: "app/login/login.html",
						controller: "loginController"
					}
				}
			}).state("layout", {
				abstract: true,
				views: {
					"layout": {
						templateUrl: "app/layout/layout.html",
						controller: "layoutController"
					}
				}
			}).state("dashboard", {
				url: "/dashboard",
				views: {
					'': {
						templateUrl: "app/dashboard/dashboard.html",
						controller: "dashboardController"
					},
					'superAdmin@dashboard': {
						templateUrl: "app/dashboard/superAdmin/dashboard.html",
						controller: "superAdminController"
					},
					'admin@dashboard': {
						templateUrl: "app/dashboard/admin/dashboard.html",
						controller: "organisationAdminController"
					},
					'dataAdmin@dashboard': {
						templateUrl: "app/dashboard/dataAdmin/dashboard.html",
						controller: "dataAdminController",
						title: 'Data Loader'
					},
					'user@dashboard': {
						templateUrl: "app/exploreData/exploreData.html",
						controller: "exploreDataController"
					}
				},
				parent: 'layout',
				activeTab: 'home',
				resolve: {
					auth: verifyAuth
				}
			}).state("exploreData", {
				url: "/exploreData",
				templateUrl: "app/exploreData/exploreData.html",
				controller: "exploreDataController",
				parent: 'layout',
				title: 'Data Explorer',
				activeTab: 'exploreData',
				hideHeader: true,
				resolve: {
					auth: verifyAuth
				}
			}).state("masterFeeds", {
				url: "/masterFeeds",
				templateUrl: "app/feeds/masterFeeds/masterFeeds.html",
				controller: "masterFeedsController",
				parent: 'layout',
				title: 'Master Feeds',
				activeTab: 'feeds',
				resolve: {
					auth: verifyAuth
				}
			}).state("orgFeeds", {
				url: "/orgFeeds",
				templateUrl: "app/feeds/orgFeeds/orgFeeds.html",
				controller: "orgFeedsController",
				parent: 'layout',
				title: 'Feeds',
				activeTab: 'feeds',
				resolve: {
					auth: verifyAuth
				}
			}).state("api", {
				url: "/api",
				templateUrl: "app/api/api.html",
				controller: "apiController",
				parent: 'layout',
				title: 'API Docs',
				activeTab: 'api',
				resolve: {
					auth: verifyAuth
				}
			});
		}
	]);

	return app;
});
