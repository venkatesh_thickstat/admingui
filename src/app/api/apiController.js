define(['controllerModules', 'angular', 'jquery', 'underscore'], function(controllerModules, angular, $, _) {
	'use strict';
	controllerModules.controller("apiController", ['$rootScope', '$scope', '$sce', '$state', '$window', '$timeout', '$location', '$log', 'configService',
		function($rootScope, $scope, $sce, $state, $window, $timeout, $location, $log, configService) {
			var token = JSON.parse($window.sessionStorage.auth).data.token;

			$scope.apiURL = $sce.trustAsResourceUrl(configService.getApiURL() + "?token=" + token);

		}
	]);
});
