/* globals location */ //location variable usage is fine in this file so relaxing the jshint check here
define(['serviceModules'], function(servicesModule) {
	'use strict';
	servicesModule.service('configService', ['$http',
		function($http) {
			var defaultProtocol = "https:",
				defaultHost = "api.dev.thickstat.ai",
				botServerHost = "bot.dev.thickstat.ai",
				defaultPort = "443",
				appURL,

				/**
				 *  Returns the application API URL
				 *
				 * @returns {string}
				 * @private
				 */
				_getApiURL = function() {
					return appURL + "/api/v1";
				},

				/**
				 * TODO
				 * It return the Application admin URL to access the application components health status
				 *
				 * @returns {string}
				 * @private
				 */
				_getAppURL = function() {
					return appURL;
				},

				_getBotServerURL = function() {
					return defaultProtocol + "//" + botServerHost + ":" + defaultPort;
				},

				/**
				 *
				 * @private
				 */
				_initializeURL = function() {
					//Initializes the app URL based on the typed URL in the browser with "https://localhost:9000" as a fallback option
					// if (typeof location !== 'undefined' && typeof location.protocol !== 'undefined' && location.protocol !== 'file:') {
					if (false) {
						appURL = location.protocol;
						if (typeof location.hostname !== 'undefined') {
							appURL = appURL.concat("//").concat(location.hostname).concat(":");
						} else {
							appURL = appURL.concat("//").concat(defaultHost).concat(":");
						}
						if (typeof location.port !== 'undefined') {
							appURL = appURL.concat(location.port);
						} else {
							appURL = appURL.concat(defaultPort);
						}
					} else {
						appURL = defaultProtocol + "//" + defaultHost + ":" + defaultPort;
					}
				};

			/*,
			 _intitiaze = function() {
			 //initializing the URL from the browser address bar
			 _initializeURL();
			 };*/

			var service = {
				getApiURL: _getApiURL,
				getAppURL: _getAppURL,
				getBotServerURL: _getBotServerURL,
				initialize: _initializeURL
			};

			return service;
		}
	]);
});
