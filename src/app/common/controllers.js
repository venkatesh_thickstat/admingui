define([
	'app/login/loginController',
	'app/layout/layoutController',
	'app/dashboard/dashboardController',
	'app/dashboard/superAdmin/superAdminController',
	'app/dashboard/admin/organisationAdminController',
	'app/dashboard/dataAdmin/dataAdminController',
	'app/api/apiController',
	'app/feeds/masterFeeds/masterFeedsController',
	'app/feeds/orgFeeds/orgFeedsController',
	'app/exploreData/exploreDataController'
], function() {
	'use strict';
});
