define(['controllerModules', 'angular', 'jquery'], function(controllerModules, angular, $) {
	'use strict';
	controllerModules.controller("layoutController", ['$scope', '$state', '$window',
		function($scope, $state, $window) {
			/*jshint ignore:start*/
			(function($) {
				function centerModal() {
					$(this).css('display', 'block');
					var $dialog = $(this).find(".modal-dialog"),
						offset = ($(window).height() - $dialog.height()) / 2,
						bottomMargin = parseInt($dialog.css('marginBottom'), 10);

					// Make sure you don't hide the top part of the modal w/ a negative margin if it's longer than the screen height, and keep the margin equal to the bottom margin of the modal
					if (offset < bottomMargin) offset = bottomMargin;
					$dialog.css("margin-top", offset);
				}

				$(document).on('show.bs.modal', '.modal', centerModal);
				$(window).on("resize", function() {
					$('.modal:visible').each(centerModal);
				});
			}($));
			/*jshint ignore:end*/

			$scope.uiState = $state;

			$scope.loggedUserName = JSON.parse($window.sessionStorage.auth).data.displayName.replace(/\w\S*/g, function(txt) {
				return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
			});
			$scope.userRole = JSON.parse($window.sessionStorage.auth).data.role;

			$scope.$on("selectedDataset", function(evnt, data) {
				$scope.selectedDataset = data;
			});

			$scope.logout = function() {
				delete $window.sessionStorage.auth;
			};

		}
	]);
});
