define(['controllerModules', 'angular', 'jquery', 'underscore'], function(controllerModules, angular, $, _) {
	'use strict';
	controllerModules.controller("superAdminController", ['$rootScope', '$scope', '$state', '$window', '$timeout', '$location', '$log', 'restService', 'blockUI', 'uiGridConstants',
		function($rootScope, $scope, $state, $window, $timeout, $location, $log, restService, blockUI, uiGridConstants) {

			//Supre Admin logic

			var orgQuery, _isEditMode = false,
				_fetchMasterFeeds = function() {
					restService.GraphQL("{AllFeeds{_id name description active}}").then(function(results) {
						$scope.feedsList = results.data.AllFeeds;
					});
				},

				_fetchAllOrgs = function() {
					$scope.orgCreateOrUpdate = false;
					blockUI.start();
					restService.GraphQL("{FindAllOrgs {_id name org_industry category user_limit website active contact_name contact_email contact_num contact_desg feeds_limit { feedid totalCount usedCount}}}").then(function(results) {
						$scope.$applyAsync(function() {
							$scope.orgs = results.data.FindAllOrgs;
						});

						blockUI.stop();
					}, function(error) {
						blockUI.stop();
					});
				},

				_mappingOrgFeeds = function(isCreateNew, selectedFeeds) {
					if ($scope.feedsList) {
						$timeout(function() {
							$('.selectpicker').selectpicker();
						}, 0);

						$('.selectpicker').on('changed.bs.select', function(e) {
							$scope.$applyAsync(function() {
								var selectedFeeds = _.map($('.selectpicker').selectpicker('val'), function(feed) {
									feed = JSON.parse(feed);
									feed.totalCount = 10; //Default value
									return feed;
								});
								$scope.addedFeeds = selectedFeeds;
							});
						});

						if (!isCreateNew) {
							_isEditMode = true;
							$scope.$applyAsync(function() {
								$scope.isEditMode = true;
							});
							var selectPickerVal = [];
							selectedFeeds = _.map(selectedFeeds, function(feed) {
								var find = _.findWhere($scope.feedsList, {
									_id: feed.feedid
								});
								feed.name = find.name;
								selectPickerVal.push(JSON.stringify(find));
								return feed;
							});

							$('.selectpicker').val(selectPickerVal);
							$('.selectpicker').selectpicker('render');
							$scope.addedFeeds = selectedFeeds;
						} else {
							_isEditMode = false;
							$scope.$applyAsync(function() {
								$scope.isEditMode = false;
							});
						}
					}
				};
			$rootScope.pageTitle = "Dashboard";
			$scope.orgGridOptions = {
				data: 'orgs',
				enableColumnMenus: false,
				columnDefs: [{
						field: '_id',
						displayName: 'Id'
					},
					{
						field: 'name',
						displayName: 'Name'
					},
					{
						field: 'user_limit',
						displayName: 'User Licenses'
					},
					{
						field: 'website',
						displayName: 'Website'
					},
					{
						field: 'contact_name',
						displayName: 'Contace Name'
					},
					{
						field: 'contact_email',
						displayName: 'Contact Email'
					},
					{
						field: 'contact_num',
						displayName: 'Contact Number'
					},
					{
						name: 'edit',
						displayName: 'Edit',
						width: '50',
						enableFiltering: false,
						enableSorting: false,
						cellTemplate: '<i class="ts-image pencil-icon" data-ng-click="grid.appScope.onEditRow(row.entity)"></i>'
					}
				],
				multiSelect: false,
				showColumnMenu: true,
				onRegisterApi: function(gridApi) {
					$scope.gridApi = gridApi;
				},
				enableFiltering: false
			};

			$scope.toggleFiltering = function() {
				$scope.orgGridOptions.enableFiltering = !$scope.orgGridOptions.enableFiltering;
				$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
			};

			$scope.onEditRow = function(data) {
				$scope.$applyAsync(function() {
					delete data.$$hashKey;
					$scope.orgObject = _.clone(data);
					$scope.orgCreateOrUpdate = true;
					_mappingOrgFeeds(false, data.feeds_limit);
				});
			};

			$scope.onOrgCreateBtnClick = function() {
				$scope.$applyAsync(function() {
					$scope.orgObject = {};
					$scope.addedFeeds = [];
					$('.selectpicker').val("");
					$('.selectpicker').selectpicker('render');
					_mappingOrgFeeds(true);
				});
			};

			$scope.createOrUpdateOrg = function() {
				var stringJson;

				blockUI.start();
				var feeds_limit = _.map($scope.addedFeeds, function(feed) {
					var obj = {
						feedid: feed._id || feed.feedid,
						totalCount: feed.totalCount
					};
					return obj;
				});
				$scope.orgObject.feeds_limit = feeds_limit;

				if (_isEditMode) {
					var id = _.clone($scope.orgObject._id);
					delete $scope.orgObject._id;
					stringJson = JSON.stringify($scope.orgObject);
					orgQuery = 'mutation {updateOrg(id: "' + id + '" ,data: ' + stringJson.replace(/\"([^(\")"]+)\":/g, "$1:") + '){_id}}';
				} else {
					stringJson = JSON.stringify($scope.orgObject);
					orgQuery = 'mutation {CreateOrg(data: ' + stringJson.replace(/\"([^(\")"]+)\":/g, "$1:") + '){_id}}';
				}

				//Create Organisation
				restService.GraphQL(orgQuery).then(function(orgResults) {
					if (!orgResults.errors) {
						$.notify({
							title: "<strong>Success:</strong> ",
							message: "Organization has been " + (_isEditMode ? "Updated." : "Created.")
						}, {
							type: "info"
						});
						_fetchAllOrgs();
					} else {
						$.notify({
							title: "<strong>Error:</strong> ",
							message: orgResults.errors[0].message
						}, {
							type: "error"
						});
					}
					blockUI.stop();
				}, function(error) {
					$scope.$applyAsync(function() {
						$.notify({
							title: "<strong>Error:</strong> ",
							message: JSON.stringify(error)
						}, {
							type: "error"
						});
						blockUI.stop();
					});

				});

			};

			_fetchAllOrgs();
			_fetchMasterFeeds();
		}
	]);
});
