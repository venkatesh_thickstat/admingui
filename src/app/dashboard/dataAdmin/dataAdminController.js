/*jshint multistr: true */
define(['controllerModules', 'angular', 'jquery', 'underscore'], function(controllerModules, angular, $, _) {
	'use strict';
	controllerModules.controller("dataAdminController", ['$rootScope', '$scope', '$state', '$window', '$interval', '$timeout', '$location', '$log', 'restService', 'blockUI', 'uiGridConstants', 'FileUploader', 'configService',
		function($rootScope, $scope, $state, $window, $interval, $timeout, $location, $log, restService, blockUI, uiGridConstants, FileUploader, configService) {

			var uploadedFilePath = "";
			var _clonedKbInfoList = [];
			$scope.isFileUploaded = false;
			$scope.isCreateNewInsight = true;
			$scope.isViewInsight = false;
			$scope.isViewStatusOfInsight = false;
			$scope.showKnwConfirmation = false;
			$scope.insightObject = {};
			$rootScope.pageTitle = "Data Management";
			$scope.categories = [{
				"name": "Finance",
				"id": "finance"
			}, {
				"name": "HR",
				"id": "hr"
			}, {
				"name": "Procurement and Spend",
				"id": "procurement_and_spend"
			}, {
				"name": "Supply Chain",
				"id": "supply_chain"
			}, {
				"name": "Order Management",
				"id": "order_management"
			}, {
				"name": "Manufacturing",
				"id": "manufacturing"
			}, {
				"name": "Marketing",
				"id": "marketing"
			}, {
				"name": "Price",
				"id": "price"
			}, {
				"name": "Sales",
				"id": "sales"
			}, {
				"name": "Service",
				"id": "service"
			}, {
				"name": "Asset",
				"id": "asset"
			}, {
				"name": "Warranty",
				"id": "warranty"
			}];
			$scope.imgList = [{
					name: "xl",
					id: "excel"
				},
				{
					name: "CSV",
					id: "csv"
				},
				{
					name: "hadoop",
					id: "hadoop"
				},
				{
					name: "IBM_DB2",
					id: "ibm"
				},
				{
					name: "ms-dynamics",
					id: "ms-dynamics"
				},
				{
					name: "oracle",
					id: "oracle"
				},
				{
					name: "oracle_CRM",
					id: "oracle_crm"
				},
				{
					name: "oracle_ERP",
					id: "oracle_erp"
				},
				{
					name: "oracle_hyperion",
					id: "oracle_hyperion"
				},
				{
					name: "oracle",
					id: "oracle"
				},
				{
					name: "postgresql",
					id: "postgresql"
				},
				{
					name: "salesforce",
					id: "salesforce"
				},
				{
					name: "SAP",
					id: "sap"
				},
				{
					name: "SQL_Server",
					id: "sql_server"
				}
			];
			$scope.knwlConfGridOptions = {
				data: 'knwlConf',
				enableColumnMenus: false,
				columnDefs: [{
						field: 'name',
						displayName: 'Column Name'
					},
					{
						field: 'type',
						displayName: 'Type',
						editableCellTemplate: 'ui-grid/dropdownEditor',
						editDropdownValueLabel: 'type',
						editDropdownOptionsArray: [{
							id: 'Metrics',
							type: 'Metrics'
						}, {
							id: 'Dim',
							type: 'Dim'
						}, {
							id: 'Date',
							type: 'Date'
						}]
					},
					{
						field: 'default',
						enableCellEdit: false,
						//ng-if="row.entity.type !== \'Dim\'"
						displayName: 'Default Metrics',
						cellTemplate: '<div class="onoffswitch margin-all-5"> \
                            <input type="checkbox" ng-model="row.entity.default" name="onoffswitch" class="onoffswitch-checkbox" id="{{row.entity.name}}" checked> \
                            <label class="onoffswitch-label" for="{{row.entity.name}}"> \
                                <span class="onoffswitch-inner"></span> \
                                <span class="onoffswitch-switch"></span> \
                            </label> \
                        </div>\
                      </div>'
					},
					{
						field: 'syn',
						displayName: 'Synonyms',
						enableCellEdit: false,
						cellTemplate: '<i class="tag-png margin-all-5 pull-left" data-ng-click="grid.appScope.addSync(row.entity)"></i> <span style="line-height: 28px;">{{ row.entity.syn | join: ", "}}</span>'
					},
					{
						field: 'description',
						displayName: 'Description'
					},
					{
						field: 'aggr',
						displayName: 'Aggregation',
						editableCellTemplate: 'ui-grid/dropdownEditor',
						editDropdownValueLabel: 'aggr',
						editDropdownOptionsArray: [{
							id: 'Sum',
							aggr: 'Sum'
						}, {
							id: 'Avg',
							aggr: 'Avg'
						}, {
							id: 'Count',
							aggr: 'Count'
						}]
					}
				],
				multiSelect: false,
				showColumnMenu: false,
				onRegisterApi: function(gridApi) {
					$scope.gridApi = gridApi;
				},
				enableFiltering: false
			};

			$scope.addSync = function(data) {
				$scope.$applyAsync(function() {
					$scope.tags = data;
					$("#sync-modal").modal();
				});
			};

			$scope.knwlConf = [{
				"type": "Metric",
				"syn": ["store key"],
				"name": "store_key",
				"default": false,
				"aggr": "Count"
			}, {
				"type": "Metric",
				"syn": ["sales item key"],
				"name": "sales_item_key",
				"aggr": "Count"
			}, {
				"type": "Metric",
				"syn": ["customer"],
				"name": "dw_customer",
				"aggr": "Count"
			}, {
				"type": "Metric",
				"syn": ["channel key"],
				"name": "channel_key",
				"aggr": "Count"
			}, {
				"type": "Metric",
				"syn": ["order source key"],
				"name": "order_src_key",
				"aggr": "Count"
			}, {
				"type": "Metric",
				"syn": ["item"],
				"name": "item_cnt",
				"aggr": "Count"
			}, {
				"type": "Metric",
				"syn": ["coupon quantity", "coupon qty", "quantity", "qty"],
				"name": "couponqty",
				"aggr": "Count"
			}, {
				"type": "Metric",
				"syn": ["gross sales", "sales", "revenue", "sold"],
				"name": "gross_sales",
				"aggr": "Sum",
				"default": true
			}, {
				"type": "Metric",
				"syn": ["discount key"],
				"name": "discount_key",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["item sub type", "sub type"],
				"name": "sales_item_sub_typ_desc",
				"default": false,
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["order source name", "order source"],
				"name": "order_src_nm",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["discount type"],
				"name": "discount_typ",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["order source code", "order code", "source code"],
				"name": "order_src_cd",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["sales item name", "item name"],
				"name": "sales_item_sname",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["store code"],
				"name": "store_cd",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["channel code"],
				"name": "channel_cd",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["order source description", "order description", "description", "desc", "order desc"],
				"name": "order_src_desc",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["discount code"],
				"name": "discount_cd",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["transaction date", "date", "order date"],
				"name": "txn_date",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["discount category"],
				"name": "discount_category",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["channel name", "channel"],
				"name": "channel_nm",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["item type code", "type code"],
				"name": "sales_item_typ_cd",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["item sub type code", "sub type code"],
				"name": "sales_item_sub_typ_cd",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["store name", "store"],
				"name": "store_nm",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["item type", "type"],
				"name": "sales_item_typ_desc",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["sales item code", "item code"],
				"name": "sales_item_cd",
				"aggr": "Count"
			}, {
				"type": "Dimension",
				"syn": ["transaction", "orders", "order"],
				"name": "transaction_id",
				"aggr": "Count"
			}];

			$scope.indexByNameKnwlConf = _.indexBy($scope.knwlConf, 'name');

			var _fetchKbInfo = function() {
				blockUI.start();
				restService.GraphQL("{kbInfo {id name description category retentionPeriod kbDict{ name desc aggr metricType type dDate dDim dMetric } kbHistory {  status  filepath  createdAt} }}").then(function(results) {
					$scope.$applyAsync(function() {
						_clonedKbInfoList = _.clone(results.data.kbInfo);
						$scope.kbInfoList = _.groupBy(results.data.kbInfo, 'category');
						$('.selectpicker').selectpicker();
						$(".image-picker").imagepicker();
					});

					blockUI.stop();
				}, function(error) {
					blockUI.stop();
				});
			};

			_fetchKbInfo();

			var uploader = $scope.uploader = new FileUploader({
				url: configService.getApiURL() + "/upload",
				headers: {
					Authorization: JSON.parse($window.sessionStorage.auth).data.token
				},
				queueLimit: 1
			});

			$scope.onSelectDatasource = function() {
				$scope.showCreateForm = true;
			};

			$scope.saveInsight = function() {
				$scope.insightObject.dataSource = "xls";
				$scope.insightObject.filepath = uploadedFilePath;
				$scope.insightObject.category = $('.selectpicker').selectpicker('val');
				var stringJson = JSON.stringify($scope.insightObject);
				blockUI.start();
				restService.GraphQL('mutation {kbInfo(data: ' + stringJson.replace(/\"([^(\")"]+)\":/g, "$1:") + '){id name}}').then(function(results) {
					$scope.$applyAsync(function() {
						$scope.showInsightViews("s", results.data.kbInfo);
						$scope.selected = results.data.kbInfo.name;
					});
					_fetchKbInfo();
					blockUI.stop();
				}, function(error) {
					blockUI.stop();
				});
			};

			$scope.cancelInsightCreation = function() {
				$scope.$applyAsync(function() {
					$scope.insightObject = {};
					$scope.isFileUploaded = false;
					$scope.showCreateForm = false;
					$scope.dataSourcevalue = "";
					$scope.isCreateNewInsight = true;
					$scope.uploader.clearQueue();
					$scope.uploader.destroy();
					$scope.selected = undefined;
					uploadedFilePath = "";
				});
			};

			var _wizardSteps = function(wizardDOM, count) {
				if (count === 1) {
					wizardDOM.find('div:nth-child(1)').addClass('active');
					$timeout(function() {
						_wizardSteps(wizardDOM, ++count);
					}, 5000);
				} else {
					var previousCount = Number(count - 1);
					var activeClass = wizardDOM.find('div:nth-child(' + previousCount + ')').hasClass('active');
					if (activeClass) {
						wizardDOM.find('div:nth-child(' + previousCount + ')').removeClass('active');
						wizardDOM.find('div:nth-child(' + previousCount + ')').addClass('complete');
						wizardDOM.find('div:nth-child(' + count + ')').addClass('active');
					}
					if (count === 2) {
						$scope.showKnwConfirmation = true;
						return;
					} else if (count > 4) {
						return;
					}

					$timeout(function() {
						_wizardSteps(wizardDOM, ++count);
					}, 5000);
				}
			};

			$scope.showInsightViews = function(state, data, index) {
				if (state === "v") {
					$scope.$applyAsync(function() {
						$scope.isViewInsight = true;
						$scope.isCreateNewInsight = false;
						$scope.isViewStatusOfInsight = false;
						$scope.kbInfoHistory = data.kbHistory;
						$scope.insightName = data.name;
						$scope.selected = index;
					});
				} else if (state === "c") {
					$scope.$applyAsync(function() {
						$scope.isCreateNewInsight = true;
						$scope.isViewInsight = false;
						$scope.dataSourcevalue = "";
						$scope.isViewStatusOfInsight = false;
					});
				} else if (state === "s") {
					$scope.$applyAsync(function() {
						$scope.isCreateNewInsight = false;
						$scope.isViewInsight = false;
						$scope.isViewStatusOfInsight = true;
						$scope.insightName = data.name;
						var wizardDOM = $(".bs-wizard");
						_wizardSteps(wizardDOM, 1);
					});
				}
			};

			//$scope.showInsightViews("s", {name: "test"});

			$scope.confirmKnwld = function() {
				$scope.showKnwConfirmation = false;
				var wizardDOM = $(".bs-wizard");
				_wizardSteps(wizardDOM, 3);
			};
			// CALLBACKS
			uploader.onSuccessItem = function(fileItem, response, status, headers) {
				// console.info('onSuccessItem', fileItem, response, status, headers);
				uploadedFilePath = response.filepath;
				$scope.isFileUploaded = true;
			};
			uploader.onErrorItem = function(fileItem, response, status, headers) {
				// console.info('onErrorItem', fileItem, response, status, headers);
			};

			$scope.checkInsightName = function(text) {
				if (text) {
					var find = _.findWhere(_clonedKbInfoList, {
						name: text
					});
					if (find) {
						return false;
					} else {
						return true;
					}
				}
			};
		}
	]);
});
