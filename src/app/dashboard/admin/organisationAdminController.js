define(['controllerModules', 'angular', 'jquery', 'underscore'], function(controllerModules, angular, $, _) {
	'use strict';
	controllerModules.controller("organisationAdminController", ['$rootScope', '$scope', '$state', '$window', '$timeout', '$location', '$log', 'restService', 'blockUI', 'uiGridConstants',
		function($rootScope, $scope, $state, $window, $timeout, $location, $log, restService, blockUI, uiGridConstants) {

			var _pristineUserData, _isEditMode = false,
				_fetchFeedsInfo = function(list) {
					restService.GraphQL('{FeedsList(ids: ' + JSON.stringify(list) + ' ) {_id name description active}}').then(function(results) {
						$scope.$applyAsync(function() {
							$scope.feedsList = results.data.FeedsList;
						});
					});
				},
				_mappingUserFeeds = function(isCreateNew, selectedFeeds) {
					if ($scope.feedsList) {
						$timeout(function() {
							$('.selectpicker').selectpicker();
						}, 0);

						if (!isCreateNew) {
							_isEditMode = true;
							selectedFeeds = _.map(selectedFeeds, function(feed) {
								return feed._id;
							});

							$('.selectpicker').val(selectedFeeds);
							$('.selectpicker').selectpicker('render');
							$scope.$applyAsync(function() {
								$scope.isEditMode = true;
							});
						} else {
							$scope.$applyAsync(function() {
								$scope.isEditMode = false;
							});
							_isEditMode = false;

						}
					}
				},
				defaultUserValue = {
					gender: "Male",
					orgid: $rootScope.orgid,
					role: "user",
					addFeeds: []
				},
				fetchUsers = function() {
					blockUI.start();
					restService.GraphQL('{FindUsersByOrg(id: "' + $rootScope.orgid + '" ) {_id fname lname display_name gender email mobile_num role designation orgid feeds {_id name description active}}}').then(function(results) {
						$scope.users = _.filter(results.data.FindUsersByOrg, function(user) {
							if (user.role !== "admin") {
								return user;
							}
						});
						blockUI.stop();
					});
				};

			//Declare scope varaiables
			$scope.userObject = _.clone(defaultUserValue);
			$scope.users = [];
			$scope.externalUser = true;
			$rootScope.pageTitle = "Dashboard";

			$scope.userGridOptions = {
				data: 'users',
				exporterMenuCsv: true,
				enableGridMenu: true,
				enableColumnMenus: false,
				columnDefs: [{
						field: '_id',
						displayName: 'User Id'
					},
					{
						field: 'fname',
						displayName: 'First Name'
					},
					{
						field: 'display_name',
						displayName: 'Display Name'
					},
					{
						field: 'email',
						displayName: 'Email'
					},
					{
						field: 'mobile_num',
						displayName: 'Mobile Number'
					},
					{
						field: 'role',
						displayName: 'User Role'
					},
					{
						field: 'designation',
						displayName: 'Designation'
					},
					{
						name: 'edit',
						displayName: 'Edit',
						width: '50',
						enableFiltering: false,
						enableSorting: false,
						cellTemplate: '<i class="ts-image pencil-icon" data-ng-click="grid.appScope.onUserEditRow(row.entity)"></i> '
					}
				],
				multiSelect: false,
				showColumnMenu: false,
				onRegisterApi: function(gridApi) {
					$scope.gridApi = gridApi;
				},
				enableFiltering: false
			};

			$scope.toggleFiltering = function() {
				$scope.userGridOptions.enableFiltering = !$scope.userGridOptions.enableFiltering;
				$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
			};

			$scope.onUserEditRow = function(data) {
				$scope.$applyAsync(function() {
					_isEditMode = true;
					_pristineUserData = _.clone(data);
					$scope.userObject = _.clone(data);
					_mappingUserFeeds(false, data.feeds);
					$("#new-user-modal").modal();
				});
			};

			$scope.reset = function() {
				$scope.$applyAsync(function() {
					_mappingUserFeeds(true);
					$('.selectpicker').val("");
					$('.selectpicker').selectpicker('render');
					$scope.userObject = _.clone(defaultUserValue);
				});
			};

			$scope.addOrUpdateUser = function() {
				var query, stringifyJson;
				blockUI.start();
				$scope.userObject.addFeeds = $('.selectpicker').selectpicker('val');

				if (_isEditMode) {
					//Edit mode
					var id = _.clone($scope.userObject._id);
					delete $scope.userObject._id;
					delete $scope.userObject.$$hashKey;
					$scope.userObject.removeFeeds = (_.difference(_.pluck(_pristineUserData.feeds, "_id"), $scope.userObject.addFeeds));
					$scope.userObject.addFeeds = (_.difference($scope.userObject.addFeeds, _.pluck(_pristineUserData.feeds, "_id")));
					$log.info("User Update addFeeds: " + JSON.stringify($scope.userObject.addFeeds));
					$log.info("User Update removeFeeds: " + JSON.stringify($scope.userObject.removeFeeds));
					delete $scope.userObject.feeds;

					stringifyJson = JSON.stringify($scope.userObject);

					query = 'mutation{UpdateUser(id: "' + id + '", data: ' + stringifyJson.replace(/\"([^(\")"]+)\":/g, "$1:") + ') { _id } }';

				} else {
					//Create Mode
					stringifyJson = JSON.stringify($scope.userObject);
					query = 'mutation{ CreateUser(data: ' + stringifyJson.replace(/\"([^(\")"]+)\":/g, "$1:") + ') { _id } }';
				}

				restService.GraphQL(query).then(function(results) {
					if (!results.errors) {
						fetchUsers();
						blockUI.stop();
						$('#new-user-modal').modal('toggle');
						$.notify({
							title: "<strong>Success:</strong> ",
							message: "User has been " + (_isEditMode ? "Updated." : "Created.")
						}, {
							type: "info"
						});
					} else {
						blockUI.stop();
						$.notify({
							title: "<strong>Error:</strong> ",
							message: results.errors[0].message
						}, {
							type: "error"
						});
					}

				});
			};

			restService.GraphQL('{ FindOrgById(id: "' + $rootScope.orgid + '") { _id user_limit feeds_limit{ feedid totalCount } } }').then(function(results) {
				if (!results.errors) {
					_fetchFeedsInfo(_.pluck(results.data.FindOrgById.feeds_limit, "feedid"));
					$scope.userLimit = results.data.FindOrgById.user_limit;
					$scope.feedsLimit = results.data.FindOrgById.feeds_limit.length;
				}
			});

			fetchUsers();

		}
	]);
});
