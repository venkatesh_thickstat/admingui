define(['serviceModules'], function(serviceModules) {
	'use strict';
	serviceModules.service('exploreDataServices', ['$q', '$log', function($q, $log) {

		var backgroundColor = [
			'rgba(7,101,148, 0.75)',
			'rgba(6,125,117, 0.75)',
			'rgba(220,233,230, 0.75)',
			'rgba(75, 192, 192, 0.75)',
			'rgba(118,194,171, 0.75)',
			'rgba(43,62,70, 0.75)',
			"rgba(128,0,0, 0.5)",
			"rgba(54, 162, 235, 0.75)",
			"rgba(255, 206, 86, 0.75)",
			"rgba(75, 192, 192, 0.75)",
			"rgba(153, 102, 255, 0.75)",
			"rgba(255, 159, 64, 0.75)"
		];

		var borderColor = [
			'rgba(7,101,148,1)',
			'rgba(6,125,117, 1)',
			'rgba(220,233,230, 1)',
			'rgba(220,233,230, 1)',
			'rgba(153, 102, 255, 1)',
			'rgba(118,194,171, 1)',
			"rgba(128,0,0,1)",
			"rgba(54, 162, 235, 1)",
			"rgba(255, 206, 86, 1)",
			"rgba(75, 192, 192, 1)",
			"rgba(153, 102, 255, 1)",
			"rgba(255, 159, 64, 1)"
		];

		this.generateChartData = function(inputData) {

			var data = inputData;
			var columns = data.columns.map((element) => {
				return element.replace(/['"]+/g, '');
			});
			var graphType = data.graph;
			var values = data.value;
			var trendCol = data.colType.date;
			var chartData = {
				"trend": {},
				"dim": {},
				"met": {}
			};

			//Get index for all the columns and set empty arry in ChartData for next data push
			var trendIdx = trendCol.map((v, i) => {
				chartData.trend[v] = [];
				return columns.indexOf(v);
			});

			var dimCol = data.colType.dim;
			var dimIdx = dimCol.map((v, i) => {
				chartData.dim[v] = [];
				return columns.indexOf(v);
			});

			//Fix for getting Metrics data
			var tempArray = dimCol.concat(trendCol);

			var metCol = columns.filter((col) => {
				return tempArray.indexOf(col) === -1;
			});

			//metCol = data["colType"]["metrics"] // uncomment after Venkat's fix
			var metIdx = metCol.map((v, i) => {
				chartData.met[v] = [];
				return columns.indexOf(v);
			});

			values.forEach(function(element) {
				metIdx.forEach((v) => {
					chartData.met[columns[v]].push(element[v]);
				});

				trendIdx.forEach((v) => {
					chartData.trend[columns[v]].push(element[v].toString());
				});

				dimIdx.forEach((v) => {
					chartData.dim[columns[v]].push(element[v].toString());
				});

			});
			return {
				data: chartData
			};
		};

		this.getChartOptions = function(chartData, chartOption) {
			var uniqueValue, metValue, colorIdx = 0;
			chartOption.options.title.text = chartData.text || "Order Summary";
			var stackData = {};
			if (chartData != null) {
				var dimCols = Object.keys(chartData.data.dim),
					dimLen = dimCols.length,
					metCols = Object.keys(chartData.data.met),
					metLen = metCols.length,
					trendCols = Object.keys(chartData.data.trend),
					trendLen = trendCols.length;

				if (trendLen === 1) {
					chartOption.type = "line";
					chartOption.data.datasets[0].fill = false;
					chartOption.data.datasets[0].borderWidth = 3;
					chartOption.data.datasets[0].backgroundColor = backgroundColor[1];
					chartOption.data.datasets[0].borderColor = borderColor[1];
					chartOption.data.labels = chartData.data.trend[trendCols[0]]; // Take the first column
					if (dimCols.length === 0) {
						chartOption.data.datasets[0].data = chartData.data.met[metCols[0]];
					} else {
						chartOption.data.datasets[0].data = chartData.data.dim[dimCols[0]];
					}

					return chartOption;
				} else if (trendLen === 2) {
					chartOption.type = "line";
					chartOption.data.labels = chartData.data.ternd[trendCols[0]]; // Take the first column
					uniqueValue = new Set(chartData.data.trend[trendCols[1]]);

					uniqueValue.forEach((label) => {
						stackData[label] = {
							"label": label,
							data: []
						};
					});
					//Currently works only for 1 metrics and 2 dimension
					metValue = chartData.data.met[metCols[0]];
					var secondTrendValue = chartData.data.trend[trendCols[1]];
					for (let row of metValue.entries()) {
						stackData[secondTrendValue[row[0]]].data.push(row[1]);
					}
					chartOption.data.datasets = [];
					for (let key in Object.keys(stackData)) {
						stackData[key].backgroundColor = backgroundColor[colorIdx];
						stackData[key].borderColor = borderColor[colorIdx];
						colorIdx = colorIdx + 1;
						chartOption.data.datasets.push(stackData[key]);
					}
					return chartOption;
				} else if (dimLen === 1) {
					chartOption.type = "bar";
					chartOption.data.labels = chartData.data.dim[dimCols[0]]; // Take the first column
					chartOption.data.datasets[0].data = chartData.data.met[metCols[0]];
					$log.info(chartOption);
					for (var i = 0; i < dimCols[0].length; i++) {
						chartOption.data.datasets[0].backgroundColor.push(backgroundColor[1]);
						chartOption.data.datasets[0].borderColor.push(backgroundColor[1]);
					}
					return chartOption;
				} else if (dimLen === 2) {
					$log.info(dimCols);
					chartOption.type = "bar";
					chartOption.data.labels = Array.from(new Set(chartData.data.dim[dimCols[0]])); // Take the first column
					uniqueValue = new Set(chartData.data.dim[dimCols[1]]);

					uniqueValue.forEach((label) => {
						stackData[label] = {
							"label": label,
							data: []
						};
					});
					//Currently works only for 1 metrics and 2 dimension
					metValue = chartData.data.met[metCols[0]];
					var secondDimValue = chartData.data.dim[dimCols[1]];
					for (let row of metValue.entries()) {
						stackData[secondDimValue[row[0]]].data.push(row[1]);
					}
					$log.info(metValue);
					$log.info(stackData);
					colorIdx = 0;
					chartOption.data.datasets = [];

					for (let key of Object.keys(stackData)) {
						$log.info("<Fill Color");
						$log.info(key);
						stackData[key].backgroundColor = backgroundColor[colorIdx];
						stackData[key].borderColor = borderColor[colorIdx];
						colorIdx = colorIdx + 1;
						chartOption.data.datasets.push(stackData[key]);
					}

					return chartOption;
				} else {
					return null;
				}

			} else {
				return null;
			}
		};
	}]);
});
