define(['controllerModules', 'angular', 'jquery', 'underscore', 'chartjs', 'app/common/lib/base64'], function(controllerModules, angular, $, _, Chart, Base64) {
	'use strict';
	controllerModules.controller("exploreDataController", ['$q', '$log', '$rootScope', '$scope', '$sce', '$compile', '$location', '$state', '$timeout', '$window', 'configService', 'restService', 'exploreDataServices', 'blockUI', '$anchorScroll',
		function($q, $log, $rootScope, $scope, $sce, $compile, $location, $state, $timeout, $window, configService, restService, exploreDataServices, blockUI, $anchorScroll) {

			var token = JSON.parse($window.sessionStorage.auth).data.token;
			var DOM_INDEX = 0;
			var _eventEmitter = function() {
				$scope.$emit("selectedDataset", $scope.selectedDataset);
			};
			var historyCountLimit = 10;
			var fetchedHistoryCount = 0;
			var isPrepend = false;
			var isReachedAllHistorys = false;
			$scope.datasets = [];
			$scope.selectedDataset = {};

			$scope.onClickDataset = function(dataSet) {
				$scope.selectedDataset = dataSet;
				_eventEmitter();
			};

			$scope.chatDateTime = new Date();

			//All DOM element variables
			var $MessageBox = $("#mesgBox");
			var $SendBtn = $(".send-btn");

			var _nameBadgeFn = function() {
				$timeout(function() {
					$('.data-set-name').nameBadge({
						border: {
							width: 0
						},
						colors: ['#FFF'],
						text: '#000',
						margin: 10,
						firstname: true,
						size: 35
					});
				}, 10);
			};

			$scope.dataetIcon = function() {
				_nameBadgeFn();
			};

			var defaultChartOptions = {
				"type": "",
				"data": {
					"labels": [],
					"datasets": [{
						"label": "",
						"data": [],
						"backgroundColor": [],
						"borderColor": [],
						"borderWidth": 1
					}]
				},
				"options": {
					"title": {
						"display": true,
						"text": "Sales"
					},
					"scales": {
						"yAxes": [{
							"ticks": {
								"beginAtZero": false
							},
							"gridLines": {
								"display": false
							}
						}],
						"xAxes": [{
							"gridLines": {
								"display": false
							}
						}]
					},
					"chartArea": {
						"backgroundColor": 'rgba(255, 255, 255, 1)'
					}
				}
			};

			var questionTemplate = '<li class="right clearfix"><span class="chat-img pull-right"><div class="data-set-name">${userName}</div></span>' +
				'<div class="chat-body clearfix"> <div class="header"> <strong class="primary-font pull-right">${userName}</strong>' +
				'</div><div class="mseg pull-right">${chatMsg}</div><div class="chat-time pull-right"> <small class="text-muted margin-top-5">' +
				'<span class="glyphicon glyphicon-time"></span>${time}</small></div></div></li>';

			var answerTemplateWithChart = '<li class="left clearfix"><span class="chat-img pull-left"><div class="data-set-name">${userName}</div></span>' +
				'<div class="chat-body clearfix"> <div class="header"> <strong class="primary-font">${userName}</strong>' +
				'</div><div class="settings"><i class="download-png" pdf-download-button pdf-name="\'Thickstat-Recommendations.pdf\'" pdf-element-id="element-to-download-${domId}" creating-pdf-class-name="\'generating\'"></i>' +
				'<i class="share-png"></i></div><div class="mseg pull-left is-graph" pdf-content="element-to-download-${domId}">' +
				'<canvas id="answerContent_${domId}"></canvas></div><div class="chat-time pull-left"> <small class="text-muted margin-top-5">' +
				'<span class="glyphicon glyphicon-time"></span>${time}</small></div></div></li>';

			var answerTemplateWithText = '<li class="left clearfix"><span class="chat-img pull-left"><div class="data-set-name">${userName}</div></span>' +
				'<div class="chat-body clearfix"> <div class="header"> <strong class="primary-font">${userName}</strong>' +
				'</div><div class="mseg pull-left" id="answerContent_${domId}"> </div><div class="chat-time pull-left"> <small class="text-muted margin-top-5">' +
				'<span class="glyphicon glyphicon-time"></span>${time}</small></div></div></li>';

			//jQuery Template caching
			$.template("quesTemplate", questionTemplate);
			$.template("ansTemplateChart", answerTemplateWithChart);
			$.template("ansTemplateText", answerTemplateWithText);

			var _question = function(data) {
				var append = isPrepend ? "prependTo" : "appendTo";
				$compile($.tmpl("quesTemplate", data))($scope)[append]("#chatRoom");
			};

			var _answerWithText = function(data) {
				var append = isPrepend ? "prependTo" : "appendTo";
				$compile($.tmpl("ansTemplateText", data))($scope)[append]("#chatRoom");
			};

			var _answerWithChart = function(data) {
				var append = isPrepend ? "prependTo" : "appendTo";
				$compile($.tmpl("ansTemplateChart", data))($scope)[append]("#chatRoom");
			};

			var _makeTimeoutCall = function(fn, data, timeout) {
				$timeout(function() {
					fn.call(null, data);
				}, timeout);
			};

			var _autoScrollContent = function(hash) {
				if ($location.hash() !== hash) {
					// set the $location.hash to `newHash` and
					// $anchorScroll will automatically scroll to it
					$location.hash(hash);
				} else {
					// call $anchorScroll() explicitly,
					// since $location.hash hasn't changed
					$anchorScroll();
				}
			};

			var _questionFromUser = function(text, isHistory) {

				_question({
					userName: $scope.loggedUserName,
					time: isHistory ? new Date(isHistory.createdAt) : new Date(),
					chatMsg: text
				});

				_nameBadgeFn();
			};

			var _answerFromServer = function(data, isHistory) {
				DOM_INDEX = DOM_INDEX + 1;
				if (data.graph) {
					_answerWithChart({
						userName: isHistory ? isHistory.domain : $scope.selectedDataset.domainName,
						time: isHistory ? new Date(isHistory.createdAt) : new Date(),
						domId: DOM_INDEX
					});
					_nameBadgeFn();
					var chartData = exploreDataServices.generateChartData(data);
					var chartOptions = JSON.parse(JSON.stringify(defaultChartOptions));
					var options = exploreDataServices.getChartOptions(chartData, chartOptions);
					$scope.$applyAsync(function() {
						var canvasDOM = $("#answerContent_" + DOM_INDEX);
						canvasDOM.parent().attr("style", "width: 65%");
						var ctx = canvasDOM[0].getContext('2d');
						new Chart(ctx, options);
						_autoScrollContent("answerContent_" + DOM_INDEX);
						$scope.typingIndicator = false;
					});
				} else {
					_answerWithText({
						userName: $scope.selectedDataset.domainName,
						time: isHistory ? new Date(isHistory.createdAt) : new Date(),
						domId: DOM_INDEX
					});
					_nameBadgeFn();
					$scope.$applyAsync(function() {
						var chartDOM = $("#answerContent_" + DOM_INDEX);
						var contentText = data.text || data;
						$(chartDOM).text(contentText);
						_autoScrollContent("answerContent_" + DOM_INDEX);
						$scope.typingIndicator = false;
					});
				}
			};

			var _answerFromServerHistory = function(ansData) {
				_answerFromServer(JSON.parse(Base64.decode(ansData.data)), {
					createdAt: ansData.createdAt,
					domain: ansData.domain
				});
			};

			var _questionFromUserHistory = function(ansData) {
				_questionFromUser(ansData.processedUtterence, {
					createdAt: ansData.createdAt
				});
			};

			var _getChatHistory = function() {

				var reqObject = {
					"dataSet": $scope.selectedDataset.id,
					"page": {
						"from": fetchedHistoryCount,
						"size": historyCountLimit
					}
				};
				blockUI.start();
				restService.customPostURL('historys', configService.getBotServerURL() + '/api/historys?token=' + token, reqObject).then(function(chatHistory) {
					if (!isPrepend)
						chatHistory.data.reverse();
					isReachedAllHistorys = (chatHistory.data.length !== historyCountLimit);
					if (chatHistory.data.length > 0) {
						_.each(chatHistory.data, function(data) {
							//Create history data with Closure mechanism
							_makeTimeoutCall(
								function(ansData) {
									if (isPrepend) {
										//In prepend add answer DOM in first posotion
										_answerFromServerHistory(ansData);
										_questionFromUserHistory(ansData);
									} else {
										_questionFromUserHistory(ansData);
										_answerFromServerHistory(ansData);
									}
								},
								data,
								0
							);
						});
						blockUI.stop();
					} else {
						blockUI.stop();
					}
				}, function(error) {
					blockUI.stop();
				});
			};

			var _fetchUserDataset = function() {
				restService.GraphQL('{userDataSet { id orgId datasetName domainName}}').then(function(userDataSet) {
					var datasets = userDataSet.data.userDataSet;
					$scope.$applyAsync(function() {
						$scope.datasets = datasets;
						$scope.selectedDataset = $scope.datasets[0];
						_eventEmitter();
						_nameBadgeFn();
						_getChatHistory();
					});
				});
			};

			var _getConverseData = function(selectedText) {
				$scope.typingIndicator = true;
				var reqObject = {
					session: {
						message: {
							text: selectedText,
							domain: $scope.selectedDataset.domainName,
							dataSet: $scope.selectedDataset.id
						},
						options: {
							transform: false,
							channel: "chat",
							source: "web"
						}
					}
				};

				restService.customPostURL('converse', configService.getBotServerURL() + '/api/converse?token=' + token, reqObject).then(function(dataSet) {
					$log.info(dataSet.response);
					var data;
					if (dataSet.status === "ok") {
						data = dataSet.response.data;
					} else {
						data = dataSet.response;
					}
					_answerFromServer(data);

				}, function(err) {
					_answerFromServer(JSON.stringify(err));
				});
			};

			$scope.search = function() {
				_getConverseData($MessageBox.val());
				_questionFromUser($MessageBox.val());
				isPrepend = false;
				$MessageBox.val("");
				$MessageBox.focus();
			};

			//Handling scroll top reached
			$scope.handleScrollToTop = function() {
				if (!isReachedAllHistorys) {
					fetchedHistoryCount = fetchedHistoryCount + historyCountLimit;
					isPrepend = true;
					_getChatHistory();
				} else {
					return;
				}
			};

			$MessageBox.easyAutocomplete({
				url: function(payload) {
					return configService.getBotServerURL() + '/api/suggestions?token=' + token;
				},
				getValue: function(data) {
					return data.text;
				},
				ajaxSettings: {
					contentType: "application/json",
					crossDomain: true,
					type: "POST",
					data: {
						suggest: {
							text: 'ord'
						}
					}
				},
				preparePostData: function(data) {
					data.suggest.text = $MessageBox.val();
					return JSON.stringify(data);
				},
				listLocation: 'data',
				list: {
					match: {
						enabled: true
					},
					onChooseEvent: function() {
						// var selectedText = $MessageBox.getSelectedItemData().text; //'order summary by store and discount category for last year';
						// _getConverseData(selectedText);
						// _questionFromUser(selectedText);
					}
				}
			});

			//Init (after page load make default function calls)
			_fetchUserDataset();
		}
	]);
});
