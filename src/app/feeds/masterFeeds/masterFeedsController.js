define(['controllerModules', 'angular', 'jquery', 'underscore'], function(controllerModules, angular, $, _) {
	'use strict';
	controllerModules.controller("masterFeedsController", ['$rootScope', '$scope', '$state', '$window', '$timeout', '$location', '$log', 'restService', 'blockUI', 'uiGridConstants',
		function($rootScope, $scope, $state, $window, $timeout, $location, $log, restService, blockUI, uiGridConstants) {
			var _fetchMasterFeeds = function() {
					restService.GraphQL("{AllFeeds{_id name description price active}}").then(function(results) {
						$scope.feedsList = results.data.AllFeeds;
					});
				},
				_isEditMode = false;
			_fetchMasterFeeds();

			$scope.onEditRow = function(data) {
				_isEditMode = true;
				$scope.$applyAsync(function() {
					delete data.$$hashKey;
					$scope.feedObject = _.clone(data);
					$("#new-feed-modal").modal();
				});
			};

			$scope.reset = function() {
				$scope.$applyAsync(function() {
					_isEditMode = false;
					$scope.feedObject = {
						active: true
					};
				});
			};

			$scope.addOrUpdateFeed = function() {
				var stringJson, feedQuery;
				blockUI.start();

				if (_isEditMode) {
					var id = _.clone($scope.feedObject._id);
					delete $scope.feedObject._id;
					stringJson = JSON.stringify($scope.feedObject);
					feedQuery = 'mutation {updateFeed(id: "' + id + '" ,data: ' + stringJson.replace(/\"([^(\")"]+)\":/g, "$1:") + '){_id}}';
				} else {
					stringJson = JSON.stringify($scope.feedObject);
					feedQuery = 'mutation {addNewFeed(data: ' + stringJson.replace(/\"([^(\")"]+)\":/g, "$1:") + '){_id}}';
				}

				restService.GraphQL(feedQuery).then(function(feedResult) {
					if (!feedResult.errors) {
						$('#new-feed-modal').modal('toggle');
						$.notify({
							title: "<strong>Success:</strong> ",
							message: "Feed has been " + (_isEditMode ? "Updated." : "Created.")
						}, {
							type: "info"
						});
						_fetchMasterFeeds();
					} else {
						$.notify({
							title: "<strong>Error:</strong> ",
							message: feedResult.errors[0].message
						}, {
							type: "error"
						});
					}
					blockUI.stop();
				}, function(error) {
					$scope.$applyAsync(function() {
						$.notify({
							title: "<strong>Error:</strong> ",
							message: error.data.errors[0].message
						}, {
							type: "error"
						});
						blockUI.stop();
					});

				});

			};

			$scope.feedGridOptions = {
				data: 'feedsList',
				enableColumnMenus: false,
				columnDefs: [{
						field: '_id',
						displayName: 'Id'
					},
					{
						field: 'name',
						displayName: 'Name'
					},
					{
						field: 'description',
						displayName: 'Description'
					},
					{
						field: 'price',
						displayName: 'Price (per user in $)'
					},
					{
						field: 'active',
						displayName: 'Active'
					},
					{
						name: 'edit',
						displayName: 'Edit',
						width: '50',
						enableFiltering: false,
						enableSorting: false,
						cellTemplate: '<i class="ts-image pencil-icon" data-ng-click="grid.appScope.onEditRow(row.entity)"></i> ',
					}
				],
				multiSelect: false,
				showColumnMenu: true,
				onRegisterApi: function(gridApi) {
					$scope.gridApi = gridApi;
				},
				enableFiltering: false
			};

			$scope.toggleFiltering = function() {
				$scope.feedGridOptions.enableFiltering = !$scope.feedGridOptions.enableFiltering;
				$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
			};

		}
	]);
});
