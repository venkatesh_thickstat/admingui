define(['controllerModules', 'angular', 'jquery', 'underscore'], function(controllerModules, angular, $, _) {
	'use strict';
	controllerModules.controller("orgFeedsController", ['$rootScope', '$scope', '$state', '$window', '$timeout', '$location', '$log', 'restService', 'blockUI', 'uiGridConstants',
		function($rootScope, $scope, $state, $window, $timeout, $location, $log, restService, blockUI, uiGridConstants) {
			var orgFeeds;

			var _fetchFeedsInfo = function(list) {
				restService.GraphQL('{FeedsList(ids: ' + JSON.stringify(list) + ' ) {_id name description price active}}').then(function(results) {
					$scope.feedsList = _.map(results.data.FeedsList, function(feed) {
						var findOrgFeed = _.findWhere(orgFeeds, {
							feedid: feed._id
						});
						feed.totalCount = findOrgFeed.totalCount;
						feed.usedCount = findOrgFeed.usedCount;
						return feed;
					});
				});
			};

			restService.GraphQL('{ FindOrgById(id: "' + $rootScope.orgid + '") { _id user_limit feeds_limit{ feedid totalCount usedCount} } }').then(function(results) {
				if (!results.errors) {
					orgFeeds = results.data.FindOrgById.feeds_limit;
					_fetchFeedsInfo(_.pluck(results.data.FindOrgById.feeds_limit, "feedid"));
					$scope.userLimit = results.data.FindOrgById.user_limit;
					$scope.feedsLimit = results.data.FindOrgById.feeds_limit.length;
				}
			});

			$scope.feedGridOptions = {
				data: 'feedsList',
				columnDefs: [{
						field: 'name',
						displayName: 'Name'
					},
					{
						field: 'description',
						displayName: 'Description'
					},
					{
						field: 'price',
						displayName: 'Price (per user in $)'
					},
					{
						field: 'totalCount',
						displayName: 'License Purchased'
					},
					{
						field: 'usedCount',
						displayName: 'License Used'
					},
					{
						field: 'active',
						displayName: 'Active'
					}
				],
				multiSelect: false,
				showColumnMenu: true,
				onRegisterApi: function(gridApi) {
					$scope.gridApi = gridApi;
				},
				enableFiltering: false
			};

		}
	]);
});
