define(['controllerModules', 'angular', 'jquery'], function(controllerModules, angular, $) {
	'use strict';
	controllerModules.controller("loginController", ['$scope', '$rootScope', '$window', '$state', '$log', 'authService', 'restService',
		function($scope, $rootScope, $window, $state, $log, authService, restService) {

			var loginResponseHandler = function(isAuthenticated, uid) {
				if (isAuthenticated) {
					$.notifyClose();
					$rootScope.loggedIn = true;
					$state.go('dashboard');
				} else {
					$rootScope.loggedIn = false;
					$.notify({
						title: "<strong>Error:</strong> ",
						message: "Invalid username/password"
					}, {
						type: "error"
					});

				}
				$scope.$applyAsync(function() {
					$scope.loginPressed = false;
				});
			};

			$scope.email = "";
			$scope.pass = "";

			$scope.login = function() {
				$scope.loginPressed = true;
				var obj = {
					email: $scope.email,
					password: $scope.pass,
					deviceId: "123456",
					deviceName: "BrowserWeb"
				};
				authService.login(obj, loginResponseHandler);
			};

		}
	]);
});
