/*global document*/
require.config({
	// baseUrl : '../src',
	// urlArgs : 'v=1.0',
	waitSeconds: 200,
	paths: {
		//Third party library paths
		'jquery.min': "../bower_components/jquery/dist/jquery.min",
		angular: "../bower_components/angular/angular",
		angularValidator: "../bower_components/tg-angular-validator/dist/angular-validator.min",
		uirouter: "../bower_components/angular-ui-router/release/angular-ui-router.min",
		underscore: "../bower_components/underscore/underscore-min",
		bootstrap: "../bower_components/bootstrap/dist/js/bootstrap.min",
		Restangular: "../bower_components/restangular/dist/restangular.min",
		angularUIGrid: "../bower_components/angular-ui-grid/ui-grid.min",
		lodash: "../bower_components/lodash/dist/lodash.min",
		ngAnimate: "../bower_components/angular-animate/angular-animate.min",
		bsNotify: "../bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min",
		bootGrid: "../bower_components/jquery.bootgrid/dist/jquery.bootgrid.min",
		bootstrapSelect: "../bower_components/bootstrap-select/dist/js/bootstrap-select.min",
		uiBlock: "../bower_components/angular-block-ui/dist/angular-block-ui.min",
		upload: "../bower_components/angular-file-upload/dist/angular-file-upload.min",
		//dragDrop: "../bower_components/angular-drag-and-drop-lists/angular-drag-and-drop-lists",
		imagePicker: "../bower_components/image-picker/image-picker/image-picker.min",
		ngBootstrap: "../bower_components/angular-bootstrap/ui-bootstrap-tpls.min",
		autoComplete: "../bower_components/EasyAutocomplete/dist/jquery.easy-autocomplete.min",
		chartjs: "../bower_components/chart.js/dist/Chart.bundle.min",
		ngTags: "../bower_components/ng-tags-input/ng-tags-input.min",
		jqueryTemplate: "./app/common/lib/jquery.tmpl.min",
		es6Promise: "../bower_components/es6-promise/es6-promise.min",
		domToImg: "../bower_components/dom-to-image/dist/dom-to-image.min",
		pdfmake: "../bower_components/pdfmake/build/pdfmake.min",
		domToPdf: "../bower_components/ng-dom-to-pdf/dist/ng-dom-to-pdf.min",
		initial: "./app/common/lib/initial",
		controllers: "app/common/controllers",
		controllerModules: "app/common/controllerModules",
		services: "app/common/services",
		serviceModules: "app/common/serviceModules"
	},

	shim: {

		'jquery.min': {
			exports: 'jQuery'
		},

		angular: {
			exports: 'angular',
			deps: ['jquery.min']
		},

		jqueryTemplate: {
			deps: ['jquery.min']
		},

		imagePicker: {
			deps: ['jquery.min']
		},

		chartjs: {
			deps: ['jquery.min'],
			exports: 'Chart'
		},

		"domToPdf": {
			deps: ['es6Promise', 'angular', 'domToImg', 'pdfmake']
		},

		initial: {
			deps: ['jquery.min']
		},

		upload: {
			deps: ['angular']
		},

		ngBootstrap: {
			deps: ['angular']
		},

		ngTags: {
			deps: ['angular']
		},

		autoComplete: {
			deps: ['jquery.min']
		},

		uiBlock: {
			deps: ['angular']
		},

		bootstrapSelect: {
			deps: ['bootstrap']
		},

		angularValidator: {
			deps: ['angular']
		},

		angularUIGrid: {
			deps: ['angular']
		},

		bootGrid: {
			deps: ['jquery.min']
		},

		bsNotify: {
			deps: ['bootstrap']
		},

		ngAnimate: {
			exports: 'ngAnimate',
			deps: ['angular']
		},

		uirouter: {
			deps: ['angular']
		},

		underscore: {
			exports: '_'
		},

		Restangular: {
			deps: ['angular', 'underscore', 'lodash']
		},

		bootstrap: {
			deps: ['jquery.min', 'angular']
		}

	}

});

require(['externalModules'], function() {
	require(["angular", "app/app"], function(angular) {
		angular.bootstrap(document.documentElement, ['tsApp'], {
			//Enabling strict DI to make sure that there are no issues with minifier
			strictDi: true
		});
	});
});
