module.exports = function(grunt) {

	// Load the plugins from package.json Dependencies
	require('load-grunt-tasks')(grunt);
	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Project configuration.
	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),

		//This takes care of doing static analysis of the java script error to find issues in the code
		jshint : {
			all : {
				options : {
					jshintrc : './.jshintrc',
					reporterOutput: ''
				},
				src : ['src/**/*.js', '!node_modules/**/*.js', '!src/app/common/lib/*.js']
			}
		},

		sass : {
			dist : {
				files : {
					'src/resources/css/index.css' : 'src/resources/sass/index.scss'
				}
			}
		},
		watch : {
			source : {
				files : ['src/resources/sass/**/*.scss'],
				tasks : ['sass'],
				options : {
					livereload : true, // needed to run LiveReload
				}
			}
		},


		//This target takes care of building/optimizing the javascript/css files
		requirejs : {
			js : {
				options : {
					baseUrl : "src",
					mainConfigFile : "src/main.js",
					wrap : true,
					//dir : "dist/build",
					name : "../node_modules/almond/almond", // assumes a production build using almond
					findNestedDependencies : true,
					removeCombined : true,
					optimizeCss : "none",
					preserveLicenseComments : false,
					optimize : "none",
					generateSourceMaps: false,
					include : ["main"],
					//name : "almond",
					out : "dist/build/main.js"
					// modules : [{
					// name : "main",
					// exclude : ["infrastructure"]
					// }, {
					// name : "infrastructure"
					// }]
				}
			},

			css : {
				options : {
					optimizeCss : "standard",
					cssIn : "src/resources/css/index.css",
					out : "dist/build/resources/css/index.css"
				}
			}
		},

		//Takes care of optimizing the image files
		imagemin : {
			dist : {
				files : [{
					expand : true,
					cwd : 'src/resources/images',
					src : '**/*.{gif,png,jpg,jpeg,svg,ico}',
					dest : 'dist/build/resources/images'
				}]
			}
		},

		//Takes care of optimizing the html files
		htmlmin : {
			dist : {
				options : {
					keepClosingSlash: true
				},
				files : [{
					expand : true,
					cwd : 'src',
					src : ['**/*.html'],
					dest : 'dist/build'
				}]
			}
		},

		//copies any files missed by requirejs target
		copy : {
			// customFonts : {
			// 	expand : true,
			// 	flatten : true,
			// 	cwd : '.',
			// 	src : 'src/resources/css/fonts/Thardt/*',
			// 	dest : 'dist/build/resources/css/fonts/Thardt'
			// },

			bootStrapfonts : {
				expand : true,
				flatten : true,
				cwd : '.',
				src : 'bower_components/bootstrap/fonts/*',
				dest : 'dist/build/resources/fonts'
			}
		},

		//This takes care making neccessary changes in the production version like changing relative paths etc.
		'string-replace' : {
			dist : {
				files : {
					"dist/build/index.html" : "dist/build/index.html",
					"dist/build/resources/css/index.css" : "dist/build/resources/css/index.css",
					"dist/build/main.js" : "dist/build/main.js"
				},
				options : {
					replacements : [{
						pattern : '<script src="../bower_components/requirejs/require.js" data-main="main"></script>',
						replacement : '<script src="./main.js" ></script>'
					}, {
						pattern : /\.\.\/\.\.\/\.\.\/bower_components\/bootstrap\/dist/gi,
						//pattern : /bootstrap/g,
						replacement : '..'
					}/*,{
						pattern : /\.\.\/css\/fonts\/Theinhardt/gi,
						replacement : 'fonts/Theinhardt'
					}*/]
				}
			}
		},

		"jsbeautifier" : {
			"beautify" : {
				src : ["src/**/*.js", "src/**/*.html"],
				options : {
					js : {
						maxPreserveNewlines : 2,
						indentWithTabs : true,
						eol : "\r\n",
						endWithNewline : true
					},
					html: {
			              braceStyle: "collapse",
			              indentChar: " ",
			              indentScripts: "keep",
			              indentSize: 4,
			              maxPreserveNewlines: 10,
			              preserveNewlines: true,
			              unformatted: ["a", "sub", "sup", "b", "i", "u"],
			              wrapLineLength: 0
			          }
				}
			},

			"git-pre-commit" : {
				src : ["src/**/*.js"],
				options : {
					mode : "VERIFY_ONLY",
					js : {
						maxPreserveNewlines : 2,
						indentWithTabs : true,
						eol : "\r\n",
						endWithNewline : true
					}
				}
			}
		},

		githooks : {
			all : {
				// Will run the jshint and test:unit tasks at every commit
				'pre-commit' : 'jshint jsbeautifier:git-pre-commit'
			}
		}

	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');

	// Default task(s).
	grunt.registerTask('default', ['sass', 'githooks', 'jshint', 'jsbeautifier:beautify', 'requirejs', 'imagemin', 'htmlmin', 'copy', 'string-replace']);
	grunt.registerTask('beautifySource', ['jsbeautifier:beautify']);
};
